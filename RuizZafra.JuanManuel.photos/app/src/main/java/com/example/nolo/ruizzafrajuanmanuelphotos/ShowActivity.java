package com.example.nolo.ruizzafrajuanmanuelphotos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by Nolo on 25/10/2016.
 */
public class ShowActivity extends Activity {
    private String src;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        Bundle bundle = getIntent().getExtras();
        src = bundle.getString("Photo");
        ImageView img = (ImageView) findViewById(R.id.ivShow);
        if(img != null) {
            new LoadImage(img).execute(src);
        }
    }

    public void finalizar(View view) {
        finish();
    }

    class LoadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        private ProgressDialog pDialog;

        public LoadImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ShowActivity.this);
            pDialog.setMessage("Loading photo ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                mIcon11 = BitmapFactory.decodeStream((InputStream) new URL(urldisplay).getContent());

            } catch (Exception e) {
                //Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (pDialog.isShowing()){
                pDialog.dismiss();
            }
            bmImage.setImageBitmap(result);
        }
    }
}