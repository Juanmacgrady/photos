package com.example.nolo.ruizzafrajuanmanuelphotos;

import android.graphics.Bitmap;

/**
 * Created by Nolo on 24/10/2016.
 */

public class Photos {

    private int id;
    private int sol;
    private String img_src;
    private String earth_date;

    private int camId;
    private String camName;
    private int rover_id;
    private String full_name;

    private int rovId;
    private String rovName;
    private String landing_date;
    private String launch_date;
    private String status;
    private int max_sol;
    private String max_date;
    private int total_photos;

    public Bitmap imagen;

    public Photos() {
        super();
    }

    public int getId() { return id;}
    public void setId(int id) { this.id = id; }

    public int getSol() { return sol;}
    public void setSol(int sol) { this.sol = sol; }

    public String getImgSrc() { return img_src; }
    public void setImgSrc(String img_src) { this.img_src = img_src; }

    public String getEarthDate() { return earth_date; }
    public void setEarthDate(String earth_date) { this.earth_date = earth_date; }



    public int getCamId() { return camId;}
    public void setCamId(int camId) { this.camId = camId; }

    public String getCamName() { return camName; }
    public void setCamName(String camName) { this.camName = camName; }

    public int getRoverId() {
        return rover_id;
    }
    public void setRoverId(int rover_id) { this.rover_id = rover_id; }

    public String getFullName() {
        return full_name;
    }
    public void setFullName(String full_name) { this.full_name = full_name; }



    public int getRovId() { return rovId;}
    public void setRovId(int rovId) { this.rovId = rovId; }

    public String getRovName() { return rovName; }
    public void setRovName(String rovName) { this.rovName = rovName; }

    public String getLanding() { return landing_date; }
    public void setLanding(String landing_date) { this.landing_date = landing_date; }

    public String getLaunch() { return launch_date; }
    public void setLaunch(String launch_date) { this.launch_date = launch_date; }

    public String getStatus() { return status; }
    public void setStatus(String status) { this.status = status; }

    public int getMaxSol() { return max_sol;}
    public void setMaxSol(int max_sol) { this.max_sol = max_sol; }

    public String getMaxDate() { return max_date; }
    public void setMaxDate(String max_date) { this.max_date = max_date; }

    public int getTotalPhotos() { return total_photos;}
    public void setTotalPhotos(int total_photos) { this.total_photos = total_photos; }

    public Bitmap getImagen() {
        // TODO Auto-generated method stub
        return imagen;
    }
}