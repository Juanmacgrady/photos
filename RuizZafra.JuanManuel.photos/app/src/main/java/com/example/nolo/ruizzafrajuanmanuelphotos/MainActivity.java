package com.example.nolo.ruizzafrajuanmanuelphotos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private ListView listaa;

    String URL = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY";
    Activity a;
    Context context;
    static ArrayList<Photos> lista;
    JSONArray photos;
    public Photos selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = new ArrayList<Photos>();
        a=this;
        context=getApplicationContext();
        listaa = (ListView) findViewById(R.id.lv);

        new GetContacts(listaa).execute();

    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {


        ListView list;
        private ProgressDialog pDialog;
        public GetContacts(ListView listaa) {
            this.list=listaa;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // CREAMOS LA INSTANCIA DE LA CLASE
            JSONParser sh = new JSONParser();
            String jsonStr = sh.makeServiceCall(URL, JSONParser.GET);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    photos = jsonObj.getJSONArray("photos");

                    // looping through All Photos
                    for (int i = 0; i < photos.length(); i++) {
                        JSONObject c = photos.getJSONObject(i);
                        int id = Integer.parseInt(c.getString("id"));
                        int sol = Integer.parseInt(c.getString("sol"));
                        String img_src = c.getString("img_src");
                        String earth_date = c.getString("earth_date");

                        //DATOS DE LA CAMARA
                        JSONObject cam = c.getJSONObject("camera");
                        int camId = Integer.parseInt(cam.getString("id"));
                        String name = cam.getString("name");
                        int rover_id = Integer.parseInt(cam.getString("rover_id"));
                        String full_name = cam.getString("full_name");

                        //DATOS DEL ROVER
                        JSONObject rov = c.getJSONObject("rover");
                        int rovId = Integer.parseInt(rov.getString("id"));
                        String rovName = rov.getString("name");
                        String landing_date = rov.getString("landing_date");
                        String launch_date = rov.getString("launch_date");
                        String status = rov.getString("status");
                        int max_sol = Integer.parseInt(rov.getString("max_sol"));
                        String max_date = rov.getString("max_date");
                        int total_photos = Integer.parseInt(rov.getString("total_photos"));

                        //CREAMOS OBJETO Y LO LLENAMOS
                        Photos e=new Photos();
                        e.setId(id);
                        e.setSol(sol);
                        e.setImgSrc(img_src);
                        e.setEarthDate(earth_date);

                        e.setCamId(camId);
                        e.setCamName(name);
                        e.setRoverId(rover_id);
                        e.setFullName(full_name);

                        e.setRovId(rovId);
                        e.setRovName(rovName);
                        e.setLanding(landing_date);
                        e.setLaunch(launch_date);
                        e.setStatus(status);
                        e.setMaxSol(max_sol);
                        e.setMaxDate(max_date);
                        e.setTotalPhotos(total_photos);

                        // adding contact to contact list
                        lista.add(e);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Esta habiendo problemas para cargar el JSON");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()){
                pDialog.dismiss();
            }
            new CargarListTask().execute();
        }
        //HILO PARA CARGAR LOS DATOS EN EL LISTVIEW
        class CargarListTask extends AsyncTask<Void,String,Adapter>{
            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                super.onPreExecute();
            }
            protected Adapter doInBackground(Void... arg0) {
                // TODO Auto-generated method stub
                try{
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                Adapter adaptador = new Adapter(a,lista);
                return adaptador;
            }

            @Override
            protected void onPostExecute(final Adapter result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);
                listaa.setAdapter(result);

                listaa.setClickable(true);
                listaa.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                            selected = result.items.get(position);

                            Intent intent = new Intent(getApplicationContext(), ShowActivity.class);
                            intent.putExtra("Photo", selected.getImgSrc());
                            startActivity(intent);



                    }
                });
            }
        }
    }
}
